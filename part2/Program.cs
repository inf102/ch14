﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace part2
{
 class WordFrequencyFramework
  {
    public event EventHandler<object>
          _load_event_handlers = delegate { } ,
          _dowork_event_handlers = delegate { } ,
          _end_event_handlers = delegate { };

    public void run(object path_to_file)
    {
      _load_event_handlers(this, path_to_file);
      _dowork_event_handlers(this, EventArgs.Empty);
      _end_event_handlers(this, EventArgs.Empty);
    }
  }

  class DataStorage
  {
    private string[] _data;
    private StopWordFilter _stop_word_filter;

    public event EventHandler<object> _word_event_handlers = delegate { };

    public DataStorage(WordFrequencyFramework wfapp,
      StopWordFilter stop_word_filter)
    {
      this._stop_word_filter = stop_word_filter;
      wfapp._load_event_handlers += __load;
      wfapp._dowork_event_handlers += __produce_words;
    }

    private void __load(object sender, object path_to_file)
    {
      using (var f = File.OpenText(path_to_file as string))
      {
        var _data = f.ReadToEnd();
        this._data = Regex.Split((_data as string).ToLower(), "[\\W_]+");
      }
    }

    public void __produce_words(object sender, object o)
    {
      foreach (var w in _data)
        if (!_stop_word_filter.is_stop_word(w))
          _word_event_handlers(this, w);
    }
  }

  class StopWordFilter
  {
    public List<string> _stop_words = null;

    public StopWordFilter(WordFrequencyFramework wfapp)
    {
      wfapp._load_event_handlers += __load;
      wfapp.run("pride_and_prejudice.txt");
    }

    private void __load(object sender, object o)
    {
      using (var f = File.OpenText("../stop_words.txt"))
        _stop_words = f.ReadToEnd().Split(',').ToList();
      _stop_words.AddRange("a b c d e f g h i j k l m n o p q r s t u v w x y z".Split(' '));
    }

    public bool is_stop_word(string word)
    {
      return _stop_words.Contains(word);
    }
  }

  class WordFrequencyCounter
  {
    private Dictionary<string, int> _word_freqs = new Dictionary<string, int>();

    public WordFrequencyCounter(WordFrequencyFramework wfapp,
      DataStorage data_storage)
    {
      data_storage._word_event_handlers += __increment_count;
      wfapp._end_event_handlers +=  __print_freqs;
    }

    private void __increment_count(object sender, object word)
    {
      if (_word_freqs.ContainsKey(word as string))
        _word_freqs[word as string]++;
      else
        _word_freqs.Add(word as string, 1);
    }

    private void __print_freqs(object sender, object o)
    {
      foreach (var pair in _word_freqs.OrderByDescending(p => p.Value).Take(25))
        Console.WriteLine($"{pair.Key}  -  {pair.Value}");
    }

    class Program
    {
        static void Main(string[] args)
        {
          var wfapp = new WordFrequencyFramework();
          var stop_word_filter = new StopWordFilter(wfapp);
          var data_storage = new DataStorage(wfapp, stop_word_filter);
          var word_freq_counter = new WordFrequencyCounter(wfapp, data_storage);
          wfapp.run(args[0]);

          Console.WriteLine("\nZ in Stop Words: "+stop_word_filter._stop_words.Count(o => o.Contains("z")));
        }
    }
  }
}
